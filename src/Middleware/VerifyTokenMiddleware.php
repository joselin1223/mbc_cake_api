<?php

namespace App\Middleware;

use Cake\Http\Client\Request;
use Cake\Http\Cookie\Cookie;
use Cake\I18n\Time;
use App\Controller\WhitelistedTokenController;
use Firebase\JWT\ExpiredException;

class VerifyTokenMiddleware
{
    public function __invoke($request, $response, $next)
    {
        $response = $next($request, $response);
        if (array_key_exists('Authorization', $request->getHeaders())) {
            if ($_SERVER['REQUEST_URI'] != '/users/logout/') {
                $this->verify($request, $response);
            }
        }
        return $response;
    }

    public function verify($request)
    {
        $headerToken = $request->getHeader('Authorization')[0];
        if ($headerToken != null) {
            $token = str_replace(['Bearer '], null, $headerToken);
            $controller = new WhitelistedTokenController();
            if (!$controller->checkToken($token)) {
                throw new ExpiredException('Invalid Token');
            }
        }
    }
}
