<?php

namespace App\Model\Validation;

use Cake\Validation\Validator;

class EmailValidator extends Validator
{
    public function validationDefault(Validator $validator)
    {
        $validator
            ->email('email')
            ->requirePresence('email', 'create')
            ->notEmptyString('email');
        return $validator;
    }
}
