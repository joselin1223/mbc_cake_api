<?php

namespace App\Model\Validation;

use Cake\Validation\Validator;

class LastnameValidator extends Validator
{
    public function __construct()
    {
        parent::__construct();
    }
    public function validationDefault(Validator $validator)
    {
        $validator
            ->scalar('lastname')
            ->requirePresence('lastname', 'create')
            ->add(
                'lastname',
                [
                    'LASTNAME_REQUIRED' => [
                        'rule' => 'notBlank',
                        'message' => __('Lastname can not be empty')
                    ],
                    'LASTNAME_INVALID_LENGTH' => [
                        'rule' => ['lengthBetween', 3, 45],
                        'message' => __('Lastname needs to be less 45 and atlest 3 characters')
                    ],
                ]
            );
        return $validator;
    }
}
