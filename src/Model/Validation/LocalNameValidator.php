<?php

namespace App\Model\Validation;

use Cake\Validation\Validator;

class LocalNameValidator extends Validator
{
    public function validationDefault(Validator $validator)
    {
        $validator
            ->scalar('local_name')
            ->maxLength('local_name', 45)
            ->requirePresence('local_name', 'create')
            ->notEmptyString('local_name');
        return $validator;
    }
}
