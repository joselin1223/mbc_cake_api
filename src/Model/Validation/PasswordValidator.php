<?php

namespace App\Model\Validation;

use Cake\Validation\Validator;

class PasswordValidator extends Validator
{
    public function validationDefault(Validator $validator)
    {
        $validator
            ->scalar('password')
            ->add(
                'password',
                [
                    'PASSWORD_INVALID_LENGTH' => [
                        'rule' => ['minLength', 8],
                        'last' => true,
                        'message' => __('Password need to be at least 8 characters long')
                    ],
                    'PASSWORD_REQUIRED_CHARACTER' => [
                        'rule' => function ($value) {
                            return $this->requiredCharacter($value);
                        },
                        'message' => __('Password must have at least 1 integer, upper and small case case letter')
                    ]
                ]
            );
        return $validator;
    }

    public function requiredCharacter($password)
    {
        $oneUpperCase = $this->oneUpperCase($password);
        $oneLowerCase = $this->oneLowerCase($password);
        $oneInteger = $this->oneInteger($password);
        if ($oneUpperCase && $oneLowerCase && $oneInteger) {
            return true;
        }
        return false;
    }

    public function oneUpperCase($password)
    {
        $uppercase = 0;
        preg_match_all("/[A-Z]/", $password, $caps_match);
        $caps_count = count($caps_match[0]);
        /** Rutrun boolean */
        return ($uppercase < $caps_count);
    }

    public function oneLowerCase($password)
    {
        $lowerCase = 0;
        preg_match_all("/[a-z]/", $password, $caps_match);
        $caps_count = count($caps_match[0]);
        /** Rutrun boolean */
        return ($lowerCase < $caps_count);
    }

    public function oneInteger($password)
    {
        $onInteger = 0;
        preg_match_all("/[0-9]/", $password, $caps_match);
        $caps_count = count($caps_match[0]);
        /** Rutrun boolean */
        return ($onInteger < $caps_count);
    }
}
