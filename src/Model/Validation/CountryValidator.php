<?php

namespace App\Model\Validation;

use Cake\Validation\Validator;

class CountryValidator extends Validator
{
    public function validationDefault(Validator $validator)
    {
        $validator
            ->scalar('country')
            ->maxLength('country', 45)
            ->requirePresence('country', 'create')
            ->notEmptyString('country');
        return $validator;
    }
}
