<?php

namespace App\Model\Validation;

use Cake\Validation\Validator;

class ZipCodeValidator extends Validator
{
    public function validationDefault(Validator $validator)
    {
        $validator
            ->scalar('zip_code')
            ->maxLength('zip_code', 45)
            ->requirePresence('zip_code', 'create')
            ->notEmptyString('zip_code');
        return $validator;
    }
}
