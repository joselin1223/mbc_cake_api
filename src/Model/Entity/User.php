<?php

namespace App\Model\Entity;

use Cake\ORM\Entity;
use Cake\Auth\DefaultPasswordHasher;

/**
 * User Entity
 *
 * @property int $id
 * @property string $username
 * @property string $password
 * @property string $firstname
 * @property string $lastname
 * @property \Cake\I18n\FrozenDate $birthday
 * @property string $lot_block
 * @property string $street
 * @property string $local_name
 * @property string $country
 * @property string $zip_code
 * @property string $email
 * @property string|null $image
 * @property string $phone_no
 * @property string $bio
 * @property string|null $activation_code
 * @property int $activated
 * @property \Cake\I18n\FrozenTime $modified
 * @property \Cake\I18n\FrozenTime $created
 *
 * @property \App\Model\Entity\PostComment[] $post_comments
 * @property \App\Model\Entity\PostLike[] $post_likes
 * @property \App\Model\Entity\Post[] $posts
 */
class User extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'username' => true,
        'password' => true,
        'firstname' => true,
        'lastname' => true,
        'birthday' => true,
        'lot_block' => true,
        'street' => true,
        'local_name' => true,
        'country' => true,
        'zip_code' => true,
        'email' => true,
        'image' => true,
        'phone_no' => true,
        'bio' => true,
        'activation_code' => true,
        'activated' => true,
        'modified' => true,
        'created' => true,
        'post_comments' => true,
        'post_likes' => true,
        'posts' => true
    ];

    /**
     * Fields that are excluded from JSON versions of the entity.
     *
     * @var array
     */
    protected $_hidden = [
        'password'
    ];
    /**
     * Hash the password before saving
     *
     * @param  [type] $value
     * @return void
     */
    protected function _setPassword($value)
    {
        if (strlen($value)) {
            $hasher = new DefaultPasswordHasher();

            return $hasher->hash($value);
        }
    }
    protected function _getFullName()
    {
        return $this->firstname . ' ' . $this->lastname;
    }
}
