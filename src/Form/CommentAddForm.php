<?php

namespace App\Form;

use App\Model\Validation\DescriptionValidator;
use App\Model\Validation\PostIdValidator;
use App\Model\Validation\UserIdValidator;
use Cake\Form\Form;
use Cake\Form\Schema;
use Cake\Validation\Validator;

/**
 * Form Validator for adding comments
 */
class CommentAddForm extends Form
{
    /**
     * Schema
     *
     * @param Schema $schema
     * @return $schema
     */
    public function _buildSchema(Schema $schema)
    {
        return $schema;
    }

    /**
     * Validator
     *
     * @param Validator $validator
     * @return $validator
     */
    public function _buildValidator(Validator $validator)
    {
        $descriptionValidator = new DescriptionValidator();
        $validator = $descriptionValidator->validationDefault($validator);

        $userIdValidator = new UserIdValidator();
        $validator = $userIdValidator->validationDefault($validator);

        $postIdValidator = new PostIdValidator();
        $validator = $postIdValidator->validationDefault($validator);

        return $validator;
    }

    /**
     * Execute
     *
     * @param array $data array
     * @return true
     */
    public function _execute(array $data)
    {
        // Send an email.
        return true;
    }

    /**
     * Set Errors
     *
     * @param [type] $errors error
     * @return void
     */
    public function _setErrors($errors)
    {
        $this->_errors = $errors;
    }
}
