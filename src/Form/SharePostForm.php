<?php

namespace App\Form;

use Cake\Form\Form;
use Cake\Form\Schema;
use Cake\Validation\Validator;
use App\Model\Validation\PostIdValidator;
use App\Model\Validation\DescriptionValidator;

/**
 * Validator for sharing a post
 */
class SharePostForm extends Form
{
    /**
     * Schema
     *
     * @param Schema $schema
     * @return $schema
     */
    public function _buildSchema(Schema $schema)
    {
        return $schema;
    }

    /**
     * Validator
     *
     * @param Validator $validator
     * @return $validator
     */
    public function _buildValidator(Validator $validator)
    {
        $postIdValidator = new PostIdValidator();
        $validator = $postIdValidator->validationDefault($validator);

        $descriptionValidator = new DescriptionValidator();
        $validator = $descriptionValidator->validationDefault($validator);

        return $validator;
    }

    /**
     * Execute
     *
     * @param array $data array
     * @return true
     */
    public function _execute(array $data)
    {
        // Send an email.
        return true;
    }

    /**
     * Set Errors
     *
     * @param [type] $errors error
     * @return void
     */
    public function _setErrors($errors)
    {
        $this->_errors = $errors;
    }
}
