<?php

namespace App\Error;

use Cake\Error\ExceptionRenderer;

/**
 * Custom error handler
 */
class AppExceptionRenderer extends ExceptionRenderer
{
    /**
     * Not found
     *
     * @param [type] $error error
     * @return $this->controller
     */
    public function missingController($error)
    {
        return $this->controller->redirect("/not-found");
    }
}
