<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;

/**
 * Followers Controller
 *
 * @property \App\Model\Table\FollowersTable $Followers
 *
 * @method \App\Model\Entity\Follower[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class FollowersController extends AppController
{
    /**
     * Follow user
     *
     * @return $this->CommonResponses
     */
    public function followUser()
    {
        if ($this->request->is('patch')) {
            $userIdFrom = $this->Token->userId();
            $userIdTo = $this->request->data('user_id');
            $fromTo = $userIdFrom . '_' . $userIdTo;
            $data = [
                'from_to' => $fromTo,
                'user_id_from' => $userIdFrom,
                'user_id_to' => $userIdTo
            ];
            if (!$this->Validator->validate($this->request->data, 'UserIdForm', [])) {
                $error = $this->Validator->errors();

                return $this->CommonResponses->validationError($error);
            }
            if (($res = $this->CheckTableId->user($userIdTo)) !== true) {
                return $res;
            }
            $followStatus = $this->Followers->checkUserFollow($data);
            if ($followStatus == 'NOT_EXISTED') {
                $user = $this->Followers->newEntity();
                $user = $this->Followers->patchEntity($user, $data);
                $this->Followers->save($user);
            } elseif ($followStatus == 'FOLLOW') {
                $message_id = 'ALREADY_FOLLOWED';
                $message = __('Already followed this user');

                return $this->CommonResponses->logicalError($message_id, $message);
            } elseif ($followStatus == 'UNFOLLOW') {
                $follow = $this->Followers->findByFromTo($fromTo)->first();
                $follow = $this->Followers->patchEntity($follow, $data);
                $follow->is_deleted = 0;
                $this->Followers->save($follow);
            }
            $message = __('Successfully followed the user');

            return $this->CommonResponses->success($message, null);
        }

        return $this->CommonResponses->methodNotAllowed();
    }

    /**
     * Unfollow user
     *
     * @return $this->CommonResponses
     */
    public function unfollowUser()
    {
        if ($this->request->is('patch')) {
            $userIdFrom = $this->Token->userId();
            $userIdTo = $this->request->data('user_id');
            $fromTo = $userIdFrom . '_' . $userIdTo;
            $data = [
                'from_to' => $fromTo,
                'user_id_from' => $userIdFrom,
                'user_id_to' => $userIdTo
            ];
            if (!$this->Validator->validate($this->request->data, 'UserIdForm', [])) {
                $error = $this->Validator->errors();

                return $this->CommonResponses->validationError($error);
            }
            if (($res = $this->CheckTableId->user($userIdTo)) !== true) {
                return $res;
            }
            $followStatus = $this->Followers->checkUserFollow($data);
            if ($followStatus == 'UNFOLLOW' || $followStatus == 'NOT_EXISTED') {
                $message_id = 'ALREADY_UNFOLLOWED';
                $message = __('Already unfollowed this user');

                return $this->CommonResponses->logicalError($message_id, $message);
            } elseif ($followStatus == 'FOLLOW') {
                $follow = $this->Followers->findByFromTo($fromTo)->first();
                $follow = $this->Followers->patchEntity($follow, $data);
                $follow->is_deleted = 1;
                $this->Followers->save($follow);
            }
            $message = __('Successfully unfollowed the user');

            return $this->CommonResponses->success($message, null);
        }

        return $this->CommonResponses->methodNotAllowed();
    }

    /**
     * Restful viewing of all following of a user with custom finder for searching
     *
     * @param [type] $user_id user
     * @return $this->CommonResponses
     */
    public function userFollowing($user_id = null)
    {
        if ($this->request->is('get')) {
            $params = $this->request->getQueryParams();
            $data = array_merge($params, ['user_id' => $user_id, 'search' => $this->request->query('search')]);
            if (!$this->Validator->validate($data, 'FollowPaginationForm', [])) {
                $error = $this->Validator->errors();

                return $this->CommonResponses->validationError($error);
            }
            if (($res = $this->CheckTableId->user($user_id)) !== true) {
                return $res;
            }
            $data = $this->paginate(
                $this->Followers
                    ->find('FollowingFields')
                    ->find('UserFollowingRelation', $data)
            );
            if ($data == 'PAGE_NOT_FOUND') {
                return $this->CommonResponses->pageNotFound();
            }
            if (count($data) == 0) {
                $message_id = 'FOLLOWING_NOT_FOUND';
                $message = __('No following found on this user record');

                return $this->CommonResponses->logicalError($message_id, $message);
            }
            $message = __('Successfully view this user following list');

            return $this->CommonResponses->success($message, $data);
        } else {
            return $this->CommonResponses->methodNotAllowed();
        }
    }

    /**
     * Restful viewing of all follower of a user with custom finder for searching
     *
     * @param [type] $user_id user
     * @return $this->CommonResponses
     */
    public function userFollower($user_id = null)
    {
        if ($this->request->is('get')) {
            $params = $this->request->getQueryParams();
            $data = array_merge($params, ['user_id' => $user_id, 'search' => $this->request->query('search')]);
            if (!$this->Validator->validate($data, 'FollowPaginationForm', [])) {
                $error = $this->Validator->errors();

                return $this->CommonResponses->validationError($error);
            }
            if (($res = $this->CheckTableId->user($user_id)) !== true) {
                return $res;
            }
            $data = $this->paginate(
                $this->Followers
                    ->find('FollowerFields')
                    ->find('UserFollowerRelation', $data)
            );
            if ($data == 'PAGE_NOT_FOUND') {
                return $this->CommonResponses->pageNotFound();
            }
            if (count($data) == 0) {
                $message_id = 'FOLLOWER_NOT_FOUND';
                $message = __('No followers found on this user record');

                return $this->CommonResponses->logicalError($message_id, $message);
            }
            $message = __('Successfully view this user follower list');

            return $this->CommonResponses->success($message, $data);
        } else {
            return $this->CommonResponses->methodNotAllowed();
        }
    }

    /**
     * Check if I this user is my following
     *
     * @param [type] $user_id user
     * @return $this->CommonResponses
     */
    public function checkFollowing($user_id = null)
    {
        if ($this->request->is('get')) {
            $data = ['user_id_to' => $user_id, 'user_id_from' => $this->Token->userId()];
            if (!$this->Validator->validate(['user_id' => $user_id], 'UserIdForm', [])) {
                $error = $this->Validator->errors();

                return $this->CommonResponses->validationError($error);
            }
            if (($res = $this->CheckTableId->user($user_id)) !== true) {
                return $res;
            }
            $status = $this->Followers->checkStatus($data);
            $message = __('Successfully check the following relationship');
            $data = ['following' => $status];

            return $this->CommonResponses->success($message, $data);
        }

        return $this->CommonResponses->methodNotAllowed();
    }

    /**
     * Check if I this user is my follower
     *
     * @param [type] $user_id user
     * @return $this->CommonResponses
     */
    public function checkFollower($user_id = null)
    {
        if ($this->request->is('get')) {
            $data = ['user_id_from' => $user_id, 'user_id_to' => $this->Token->userId()];
            if (!$this->Validator->validate(['user_id' => $user_id], 'UserIdForm', [])) {
                $error = $this->Validator->errors();

                return $this->CommonResponses->validationError($error);
            }
            if (($res = $this->CheckTableId->user($user_id)) !== true) {
                return $res;
            }
            $status = $this->Followers->checkStatus($data);
            $message = __('Successfully check the follower relationship');
            $data = ['following' => $status];

            return $this->CommonResponses->success($message, $data);
        }

        return $this->CommonResponses->methodNotAllowed();
    }

    /**
     * This is for getting all total follower of a user
     *
     * @param [type] $user_id user id
     * @return $this->CommonResponses
     */
    public function totalFollower($user_id = null)
    {
        if ($this->request->is('get')) {
            if (!$this->Validator->validate(['user_id' => $user_id], 'UserIdForm', [])) {
                $error = $this->Validator->errors();

                return $this->CommonResponses->validationError($error);
            }
            if (($res = $this->CheckTableId->user($user_id)) !== true) {
                return $res;
            }
            $total = $this->Followers
                ->find()
                ->where(['user_id_to' => $user_id])
                ->count();
            $message = __('Successfully  get the total Follower');
            $data = ['total' => $total];

            return $this->CommonResponses->success($message, $data);
        }

        return $this->CommonResponses->methodNotAllowed();
    }

    /**
     * This is for getting all total following of a user
     *
     * @param [type] $user_id user id
     * @return $this->CommonResponses
     */
    public function totalFollowing($user_id = null)
    {
        if ($this->request->is('get')) {
            if (!$this->Validator->validate(['user_id' => $user_id], 'UserIdForm', [])) {
                $error = $this->Validator->errors();

                return $this->CommonResponses->validationError($error);
            }
            if (($res = $this->CheckTableId->user($user_id)) !== true) {
                return $res;
            }
            $total = $this->Followers
                ->find()
                ->where(['user_id_from' => intval($user_id)])
                ->count();
            $message = __('Successfully  get the total Following');
            $data = ['total' => $total];

            return $this->CommonResponses->success($message, $data);
        }

        return $this->CommonResponses->methodNotAllowed();
    }
}
