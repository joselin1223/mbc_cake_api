<?php

namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\Controller\ComponentRegistry;
use Cake\ORM\TableRegistry;

/**
 * Responses component
 */
class CheckTableIdComponent extends Component
{
    public $components = ['CommonResponses'];

    /**
     * Check the post id if existing
     *
     * @param [type] $id int
     * @return void
     */
    public function user($id = null)
    {
        $user = TableRegistry::get('Users')
            ->findById(intval($id))
            ->where(['activated' => 1])
            ->first();
        if (!$user) {
            $message_id = 'USER_NOT_FOUND';
            $message = __('User not found in the table');
            return $this->CommonResponses->logicalError($message_id, $message);
        }

        return true;
    }

    /**
     * Check if the user id is existing
     *
     * @param [type] $id int
     * @return void
     */
    public function post($id = null)
    {
        $post = TableRegistry::get('Posts')
            ->findById(intval($id))
            ->where(['is_deleted' => 0])
            ->first();
        if (!$post) {
            $message_id = 'POST_NOT_FOUND';
            $message = __('Post not found in the table');
            return $this->CommonResponses->logicalError($message_id, $message);
        }

        return true;
    }
}
