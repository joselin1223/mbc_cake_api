<?php

namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Event\Event;
use Cake\I18n\I18n;

I18n::setLocale('ja_JP');
class AppController extends Controller
{
    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('Cookie', ['expires' => '30 day']);
        $this->loadComponent('CommonResponses');
        $this->loadComponent('Validator');
        $this->loadComponent('Token');
        $this->loadComponent('CheckTableId');
        $this->loadComponent('RequestHandler');
        $this->loadComponent(
            'BryanCrowe/ApiPagination.ApiPagination',
            [
                'key' => 'paging',
                'aliases' => [
                    'page' => 'currentPage',
                    'current' => 'resultCount'
                ],
                'visible' => [
                    'limit',
                    'pageCount',
                    'currentPage',
                    'resultCount',
                    'prevPage',
                    'nextPage'
                ]
            ]
        );
        $this->loadComponent(
            'Auth',
            [
                'storage' => 'Memory',
                'authenticate' => [
                    'Form' => [],
                    'ADmad/JwtAuth.Jwt' => [
                        'parameter' => 'token',
                        'userModel' => 'Users',
                        'fields' => [
                            'username' => 'id'
                        ],
                        'queryDatasource' => true
                    ]
                ],
                'unauthorizedRedirect' => false,
                'checkAuthIn' => 'Controller.initialize'
            ]
        );
    }
}
