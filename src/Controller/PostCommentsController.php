<?php

namespace App\Controller;

use App\Controller\AppController;

/**
 * PostComments Controller
 *
 * @property \App\Model\Table\PostCommentsTable $PostComments
 *
 * @method \App\Model\Entity\PostComment[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class PostCommentsController extends AppController
{
    /**
     * This is for post creation
     *
     * @return $this->CommonResponses
     */
    public function addComment()
    {
        if ($this->request->is('post')) {
            $user_id = $this->Token->userId();
            $params = $this->request->data();
            $data = array_merge($params, ['user_id' => $user_id]);
            $comment = $this->PostComments->newEntity();
            $comment = $this->PostComments->patchEntity($comment, $data);
            if (!$this->Validator->validate($data, 'CommentAddForm', $comment->getErrors())) {
                $error = $this->Validator->errors();

                return $this->CommonResponses->validationError($error);
            }
            if (($res = $this->CheckTableId->post($data['post_id'])) !== true) {
                return $res;
            }
            if ($this->PostComments->save($comment)) {
                $message = __('Comment successfully added');

                return $this->CommonResponses->success($message, null);
            }

            return $this->CommonResponses->NotFound();
        }

        return $this->CommonResponses->methodNotAllowed();
    }

    /**
     * For viewing a post
     *
     * @param [type] $post_id post id
     * @return $this->CommonResponses
     */
    public function viewComment($post_id = null)
    {
        if ($this->request->is('get')) {
            $params = $this->request->getQueryParams();
            $data = array_merge($params, ['post_id' => $post_id]);
            if (!$this->Validator->validate($data, 'CommentPaginationForm', [])) {
                $error = $this->Validator->errors();

                return $this->CommonResponses->validationError($error);
            }
            if (($res = $this->CheckTableId->post($post_id)) !== true) {
                return $res;
            }
            $this->paginate = [
                'conditions' => ['PostComments.post_id' => $post_id]
            ];
            $posts = $this->paginate(
                $this->PostComments->find('CommentsFields')->contain('Users')
            );
            if ($posts == 'PAGE_NOT_FOUND') {
                return $this->CommonResponses->pageNotFound();
            }
            $message = __('Successfully view all comment in this post');
            $data = $posts;
            return $this->CommonResponses->success($message, $data);
        } else {
            return $this->CommonResponses->methodNotAllowed();
        }
    }
}
