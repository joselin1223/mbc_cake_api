<?php

namespace App\Controller;

use App\Controller\AppController;

/**
 * WhitelistedToken Controller
 *
 * @property \App\Model\Table\WhitelistedTokenTable $WhitelistedToken
 *
 * @method \App\Model\Entity\WhitelistedToken[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class WhitelistedTokenController extends AppController
{
    /**
     * This is for inserting token in the database
     *
     * @param [type] $token token generated in the login
     * @param [type] $issuedAt date created
     * @param [type] $expiredAt expiration date
     * @return true||false
     */
    public function add($token, $issuedAt, $expiredAt)
    {
        $data = [
            'id' => $token,
            'issue_at' => $issuedAt,
            'expired_at' => $expiredAt
        ];
        $whitelisted = $this->WhitelistedToken->newEntity();
        $whitelisted = $this->WhitelistedToken->patchEntity($whitelisted, $data);
        $whitelisted = $this->WhitelistedToken->save($whitelisted);
        if ($whitelisted) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Delete expired token
     *
     * @return void
     */
    public function deleteExpiredToken()
    {
        $currentDate = '';
        $entity = $this->WhitelistedToken->find()
            ->where(['expired_at <' => $currentDate])
            ->all();
        $this->WhitelistedToken->delete($entity);
    }

    /**
     * Destroy token stored in database
     *
     * @param [type] $token string
     * @return true||false
     */
    public function destroyToken($token)
    {
        $entity = $this->WhitelistedToken->findById($token)->first();
        if ($entity != null) {
            $this->WhitelistedToken->delete($entity);

            return true;
        } else {
            return false;
        }
    }

    /**
     * Check token
     *
     * @param [type] $token string
     * @return true||false
     */
    public function checkToken($token)
    {
        $currentDate = date("Y-m-d h:m A");
        $checkExpiration = $this->WhitelistedToken->findById($token)
            ->first();
        if ($checkExpiration != null && strtotime($checkExpiration->expired_at) > strtotime($currentDate)) {
            return true;
        } else {
            return false;
        }
    }
}
